import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import morgan = require('morgan');

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // activates validation with transform and whitelist for all routes
  // whitelist make sure that no parameter other than that listed in dto will pass through
  app.useGlobalPipes(new ValidationPipe({ transform: true, whitelist: true }));
  app.use(morgan('dev'));

  const port = 3001;
  await app.listen(port);
  console.log(`Listening On Port ${port}`);
}

bootstrap();
