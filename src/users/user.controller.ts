import { Body, Controller, Delete, Get, HttpCode, HttpStatus, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { UserService } from './user.service';
import { UserDto } from './dto/user.dto';
import { UserPutDto } from './dto/user-put.dto';

@Controller('user')
export class UserController {

  constructor(private service: UserService) {
  }


  @Get('')
  async getUsers(): Promise<UserDto[]> {
    return this.service.getUsers();
  }

  @Get(':id')
  async getById(@Param('id', ParseIntPipe) id: number): Promise<UserDto> {

    return await this.service.getOne(id);
  }

  @Post('')
  async post(@Body() body: UserDto): Promise<UserDto> {
    return this.service.post(body);
  }


  @Put(':id')
  async put(@Param('id', ParseIntPipe) id: number,
            @Body() body: UserPutDto): Promise<UserDto> {
    return this.service.put(id, body);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async delete(@Param('id', ParseIntPipe) id: number) {
    await this.service.delete(id);
  }

}
