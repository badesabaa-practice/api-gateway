import { UserDto } from './dto/user.dto';

export const usersMock: UserDto[] = [{
  id: 1,
  email: 's.hamid.sajjadi@gmail.com',
  username: 'hamids',
  name: 'hamid sajjadi',
  phone: '09014827233',
},{
  id: 2,
  email: 's.hamid2@yahoo.com',
  username: 'ham2',
  name: 'hamid sajedi',
  phone: '09123456789',
}];
