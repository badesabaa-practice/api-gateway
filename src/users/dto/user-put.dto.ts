import { IsEmail, IsOptional, Matches, MinLength } from 'class-validator';

export class UserPutDto {


  @IsOptional()
  @MinLength(3)
  username: string;

  @MinLength(2)
  @IsOptional()
  name?: string;

  @IsOptional()
  @IsEmail()
  email: string;

  @IsOptional()
  @Matches(/^(\+98|0)?9\d{9}$/, { message: 'phone format is not correct' }) // persian phone number regex
  phone: string;
}

