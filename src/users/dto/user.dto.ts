import { IsEmail, IsOptional, Matches, MinLength } from 'class-validator';

export class UserDto {

  id: number;

  @MinLength(3)
  username: string;

  @MinLength(2)
  @IsOptional()
  name?: string;

  @IsEmail()
  email: string;

  @Matches(/^(\+98|0)?9\d{9}$/, { message: 'phone format is not correct' }) // persian phone number regex
  phone: string;
}

