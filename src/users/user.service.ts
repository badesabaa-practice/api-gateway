import { Injectable, NotFoundException } from '@nestjs/common';
import { usersMock } from './user.mock';
import { UserDto } from './dto/user.dto';
import { UserPutDto } from './dto/user-put.dto';

@Injectable()
export class UserService {


  /**
   * returns list of users
   */
  async getUsers(): Promise<UserDto[]> {
    return usersMock;
  }


  /**
   * finds and returns a user with an specific id, if not found throws 404 error
   * @param id
   */
  async getOne(id: number): Promise<UserDto> {
    const user = usersMock.find(user => user.id === id);
    if (!user) {
      throw new NotFoundException;
    }
    return user;
  }


  /**
   * given a user id, finds and updates it. if user is not found throws 404 error
   * @param id
   * @param {UserPutDto} data object containing new user data
   */
  async put(id: number, data: UserPutDto): Promise<UserDto> {
    const user = await this.getOne(id);
    if (!user) {
      return null;
    }
    const temp = Object.assign(user, data);
    return temp;
  }


  /**
   * adds a new user to existing ones and returns saved object
   * @param {UserDto} data new user data
   */
  async post(data: UserDto): Promise<UserDto> {
    const newUser = Object.assign(new UserDto(), data);

    // TODO: add functionality to save new user

    return newUser;
  }

  /**
   * given an id, finds a user with it and (probably soft) removes it. if user is not found throws 404 error
   * @param id
   */
  async delete(id: number): Promise<boolean> {
    const user = await this.getOne(id);
    // TODO: actually delete user
    return true;
  }
}
