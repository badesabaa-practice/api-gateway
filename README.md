## Description

API gateway for badesabaa practice

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

Then check localhost:3001

## Postman Collection

You can find the postman collection file in `postman` folder
